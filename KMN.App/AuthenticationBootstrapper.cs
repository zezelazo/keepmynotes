﻿using Nancy;
using Nancy.Conventions;

namespace KMN.App.Api {
  public class AuthenticationBootstrapper : DefaultNancyBootstrapper {
    protected override void ConfigureConventions(NancyConventions nancyConventions) {
      base.ConfigureConventions(nancyConventions);
      nancyConventions.StaticContentsConventions.Add(StaticContentConventionBuilder.AddDirectory("/App"));
    }

    //  protected override void ApplicationStartup(TinyIoCContainer container, IPipelines pipelines)
    //  {
    //    base.ApplicationStartup(container, pipelines);

    //    pipelines.BeforeRequest += (ctx) =>
    //    {
    //      string cookie;
    //      ctx.Request.Cookies.TryGetValue("utoken", out cookie);
    //      if (!string.IsNullOrEmpty(cookie)) ctx.CurrentUser = new DemoUserIdentity { UserName = cookie, Claims = BuildClaims(cookie) };
    //      AppConfig.Instance.SetCurrentUser(new Guid(cookie));
    //      return null;
    //    };

    //    pipelines.AfterRequest += (ctx) =>
    //    {
    //      // If status code comes back as Unauthorized then
    //      // forward the user to the login page
    //      if (ctx.Response.StatusCode == HttpStatusCode.Unauthorized)
    //      {
    //        var error = new JsonResponse<ApiError>(new ApiError
    //        {
    //          ErrorCode = 401,
    //          ErrorMessage = "user is not authenticated",
    //          ErrorName = "unauthorized"
    //        },
    //                                                        new DefaultJsonSerializer())
    //                                                        {
    //                                                          StatusCode = HttpStatusCode.Unauthorized
    //                                                        };
    //        ctx.Response = error;
    //      }
    //    };
    //  }

    //  static IEnumerable<string> BuildClaims(string userName)
    //  {
    //    var claims = new List<string>();
    //    if (String.Equals(userName, "0786-0604-1008-2708-0208", StringComparison.InvariantCultureIgnoreCase)) claims.Add("sysadmin");
    //    return claims;
    //  }
  }
}
