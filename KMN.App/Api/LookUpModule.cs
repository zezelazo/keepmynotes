﻿using KMN.Common.Services;
using Nancy;

namespace KMN.App.Api {
  public class LookUpModule : NancyModule {
    readonly ILookUpsService _lookUp;

    public LookUpModule() : base("/api/lookup") {
      Get["/users"] = _ => Response.AsJson(_lookUp.GetUsers());
      Get["/elements"] = _ => Response.AsJson(_lookUp.GetElements()); 
      Get["/tags"] = _ => Response.AsJson(_lookUp.GetTags());
    }
  }
}
