﻿using KMN.App.Api.Others;
using KMN.Common.Services;
using Nancy;
using Nancy.ModelBinding;

namespace KMN.App.Api {
  public class AuthenticateModule : NancyModule {
    readonly IUserService _userService;

    public AuthenticateModule(IUserService userService) : base("/api/login") {
      _userService = userService;
      Post["/"] = parameters => {
                    var response = new Response {StatusCode = HttpStatusCode.Unauthorized};
                    var reqData = this.Bind<LoginRequest>();
                    var usr = _userService.GetUserByName(reqData.UserName);
                    if(usr == null || usr.UserName != reqData.UserName) return response;
                    if(!_userService.VerifiedPassword(usr.Id,reqData.Password)) return response;
                    response = new Response {StatusCode = HttpStatusCode.OK};
                    response.AddCookie("utoken",usr.Id.ToString());
                    return response;
                  };
      Get["/"] = parameters => {
                   var response = new Response {StatusCode = HttpStatusCode.OK};
                   response.AddCookie("utoken","0000-0000-0000-0000-0000");
                   return response;
                 };
    }
  }
}
