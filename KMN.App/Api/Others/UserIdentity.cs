﻿using System.Collections.Generic;
using Nancy.Security;

namespace KMN.App.Api.Others {
  public class DemoUserIdentity : IUserIdentity {
    #region IUserIdentity Members
    public string UserName { get; set; }
    public IEnumerable<string> Claims { get; set; }
    #endregion
  }
}
