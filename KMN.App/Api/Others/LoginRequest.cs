﻿namespace KMN.App.Api.Others {
  public class LoginRequest {
    public string UserName { get; set; }
    public string Password { get; set; }
  }
}
