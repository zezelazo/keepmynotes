﻿namespace KMN.App.Api.Others {
  public class ApiError
  {
    public string ErrorMessage { get; set; }
    public int ErrorCode { get; set; }
    public string ErrorName { get; set; }
  }
}