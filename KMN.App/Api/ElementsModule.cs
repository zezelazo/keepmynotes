﻿using System;
using System.Collections.Generic;
using KMN.Common.Entities;
using KMN.Common.Services;
using Nancy;

namespace KMN.App.Api {
  public class ElementsModule : NancyModule {
    readonly IElementsService _elements;

    public ElementsModule(IElementsService elements) : base("api/elements") {
      _elements = elements;
      //this.RequiresAuthentication();   
      Get["/id"] = x => GetElement(x.id).ToString();
    }

    public IEnumerable<Element> GetAll() {
      return _elements.GetElements(null);
    }

    public Element GetElement(Guid id) {
      return _elements.GetElementById(id);
    }
  }
}
