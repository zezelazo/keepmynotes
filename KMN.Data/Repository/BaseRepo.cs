﻿using System;
using System.Collections.Generic;
using System.Linq;
using KMN.Common;
using KMN.Common.Data.Repository;
using KMN.Common.Data.Specification;
using KMN.Common.Entities.Base;
using KMN.Common.Entities.Enums;
using KMN.Data.Context;

namespace KMN.Data.Repository {
  public abstract class BaseRepo<TKey, TEntity> : IBaseRepo<TKey, TEntity> where TEntity : Entity<TKey> {
    internal readonly IDomainContext Context;

    #region Constructor

    protected BaseRepo(IDomainContext context ) {
      Context = context;
    }

    #endregion

    internal virtual IQueryable<TEntity> Recordset {
      get { return Context.Set<TEntity>().AsNoTracking(); }
    }

    #region IBaseRepo<TKey,TEntity> Members

    public TEntity GetById(TKey id) {
      var spec = new DirectSpecification<TEntity>(e => EqualityComparer<TKey>.Default.Equals(e.Id, id));
      var result = Find(spec).First();
      return result ?? Activator.CreateInstance<TEntity>();
    }

    public IEnumerable<TEntity> GetAll() {
      return Recordset.OrderBy(i => i.Id);
    }

    public IEnumerable<TEntity> Find(ISpecification<TEntity> specification) {
      return Recordset.Where(specification.SatisfiedBy).OrderBy(i => i.Id);
    }

    public long Count(ISpecification<TEntity> specification = null) {
      return specification == null ? Recordset.Count() : Recordset.Count(specification.SatisfiedBy);
    }

    public TKey Add(TEntity item) {
      if(item == null) throw new ArgumentNullException("item", "you must add an item");
      item.State = EState.Added;
      item.Creator = AppConfig.Instance.CurrentUserId;
      item.Creation = DateTime.Now;
      //to avoid nulls
      item.LasModification = item.Creation;
      item.LastEditor = item.Creator;

      Context.ApplyChanges(item);
      return item.Id;
    }

    public bool Update(TEntity item) {
      if(item == null) throw new ArgumentNullException("item", "you must send an item to update");
      item.State = EState.Modified;
      item.LastEditor = AppConfig.Instance.CurrentUserId;
      item.LasModification = DateTime.Now;
      var rpta = Context.ApplyChanges(item);
      return rpta > 0;
    }

    public bool Delete(TKey id) {
      var deletedItem = GetById(id);
      if(deletedItem == null) return false;
      deletedItem.State = EState.Deleted;
      var rpta = Context.ApplyChanges(deletedItem);
      return rpta > 0;
    }

    #endregion
  }
}