﻿using System;
using System.Linq;
using KMN.Common;
using KMN.Common.Data.Repository;
using KMN.Common.Data.Specification;
using KMN.Common.Entities;
using KMN.Data.Context;

namespace KMN.Data.Repository {
  public class TagsRepo : BaseRepo<long, Tag>, ITagsRepo {
    private readonly DomainContext _realContext;

    public TagsRepo(IDomainContext context) : base(context) {
      _realContext = (DomainContext) context;
    }

    internal override IQueryable<Tag> Recordset {
      get { return _realContext.Tags; }
    }

    public Tag GetByName(string name) {
      return Find(new DirectSpecification<Tag>(t => string.Compare(t.Name, name.Trim(), StringComparison.InvariantCultureIgnoreCase) == 0)).FirstOrDefault();
    }
  }
}