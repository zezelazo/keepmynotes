﻿using System;
using System.Data.Entity;
using System.Linq;
using KMN.Common;
using KMN.Common.Data.Repository;
using KMN.Common.Entities;
using KMN.Data.Context;

namespace KMN.Data.Repository {
  public class UsersRepo : BaseRepo<Guid, User>, IUsersRepo {
    private readonly DomainContext _realContext;

    public UsersRepo(IDomainContext context) : base(context) {
      _realContext = (DomainContext) context;
    }

    internal override IQueryable<User> Recordset {
      get { return _realContext.Users.Include(u => u.Permisions); }
    }
  }
}