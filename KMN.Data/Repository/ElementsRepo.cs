﻿using System;
using System.Data.Entity;
using System.Linq;
using KMN.Common;
using KMN.Common.Data.Repository;
using KMN.Common.Entities;
using KMN.Data.Context;

namespace KMN.Data.Repository {
  public class ElementsRepo : BaseRepo<Guid, Element>, IElementsRepo {
    private readonly DomainContext _realContext;

    public ElementsRepo(IDomainContext context) : base(context) {
      _realContext = (DomainContext) context;
    }

    internal override IQueryable<Element> Recordset {
      get { return _realContext.Elements.Include(e => e.Properties).Include(e => e.UserPermisions); }
    }
  }
}