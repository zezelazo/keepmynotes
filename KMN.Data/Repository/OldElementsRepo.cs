﻿using System.Linq;
using KMN.Common;
using KMN.Common.Data.Repository;
using KMN.Common.Entities;
using KMN.Data.Context;

namespace KMN.Data.Repository {
  public class OldElementsRepo : BaseRepo<long, OldElement>, IOldElementsRepo {
    private readonly DomainContext _realContext;

    public OldElementsRepo(IDomainContext context) : base(context) {
      _realContext = (DomainContext) context;
    }

    internal override IQueryable<OldElement> Recordset {
      get { return _realContext.OldElements; }
    }
  }
}