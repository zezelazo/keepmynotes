﻿using System.Data.Entity;
using KMN.Common.Data.Context;

namespace KMN.Data.Context {
  public interface IDomainContext : IContext {
    DbSet<TEntity> Set<TEntity>() where TEntity : class;
    int ApplyChanges<TEntity>(TEntity root) where TEntity : class;
  }
}