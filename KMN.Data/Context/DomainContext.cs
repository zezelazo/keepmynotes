﻿using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using KMN.Common;
using KMN.Common.Entities;
using KMN.Common.Entities.Base;
using KMN.Common.Entities.Enums;

namespace KMN.Data.Context {
  public class DomainContext : DbContext, IDomainContext {
    public DomainContext():base(AppConfig.Instance.ConnectionString)
    {
      ((IObjectContextAdapter) this).ObjectContext.ObjectMaterialized += (sender, args) => {
        var entity = args.Entity as IObjectWithState;
        if(entity != null) entity.State = EState.Unchanged;
      };
    }

    public DbSet<Tag> Tags { get; set; }
    public DbSet<Element> Elements { get; set; }
    public DbSet<OldElement> OldElements { get; set; }
    public DbSet<User> Users { get; set; }
    public DbSet<ElementProperty> ElementProperties { get; set; }
    public DbSet<UserPermisions> UserPermisions { get; set; }

    #region IDomainContext Members

    public int ApplyChanges<TEntity>(TEntity root) where TEntity : class {
      using(var ctx = new DomainContext()) {
        if(ctx.Entry(root).State == EntityState.Detached) ctx.Set<TEntity>().Add(root);
        foreach(var entry in ctx.ChangeTracker.Entries<IObjectWithState>()) {
          var stateInfo = entry.Entity;
          entry.State = ctx.ConvertState(stateInfo.State);
        }
        return ctx.SaveChanges();
      }
    }

    #endregion

    protected override void OnModelCreating(DbModelBuilder modelBuilder) {
      modelBuilder.Entity<User>().Ignore(u => u.State);
      modelBuilder.Entity<Element>().Ignore(p => p.State);
      modelBuilder.Entity<OldElement>().Ignore(p => p.State);
      modelBuilder.Entity<Tag>().Ignore(p => p.State);
      modelBuilder.Entity<ElementProperty>().Ignore(p => p.State);
      modelBuilder.Entity<UserPermisions>().Ignore(p => p.State);
    }

    private EntityState ConvertState(EState state) {
      switch(state) {
        case EState.Added:
          return EntityState.Added;
        case EState.Modified:
          return EntityState.Modified;
        case EState.Deleted:
          return EntityState.Deleted;
        default:
          return EntityState.Unchanged;
      }
    }
  }
}