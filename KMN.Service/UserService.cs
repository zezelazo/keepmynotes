﻿using System;
using System.Collections.Generic;
using System.Linq;
using KMN.Common.Data.Repository;
using KMN.Common.Data.Specification;
using KMN.Common.Entities;
using KMN.Common.Services;

namespace KMN.Service {
  public class UserService : BaseService,IUserService {
    readonly IUsersRepo _usersRepo;

    public UserService(IUsersRepo usersRepo) {
      _usersRepo = usersRepo;
    }

    #region IUserService Members
    public List<User> GetUsers(ISpecification<User> filter) {
      return _usersRepo.Find(filter).ToList();
    }

    public User GetUserById(Guid id) {
      return _usersRepo.GetById(id);
    }

    public User GetUserByName(string userName) {
      var users = _usersRepo.Find(new DirectSpecification<User>(u => u.UserName == userName));
      return !users.Any() ? new User() : users.FirstOrDefault();
    }

    public Guid AddUser(User item) {
      return _usersRepo.Add(item);
    }

    public bool EditUser(User item) {
      return _usersRepo.Update(item);
    }

    public bool DisableUser(Guid id) {
      var usr = _usersRepo.GetById(id);
      if(usr == null) return false;
      usr.Enabled = false;
      return _usersRepo.Update(usr);
    }

    public bool EnableUser(Guid id) {
      var usr = _usersRepo.GetById(id);
      if(usr == null) return false;
      usr.Enabled = true;
      return _usersRepo.Update(usr);
    }

    public bool DeleteUser(Guid id) {
      return _usersRepo.Delete(id);
    }

    public bool VerifiedPassword(Guid id,string password) {
      var usr = _usersRepo.GetById(id);
      if(usr == null) return false;
      return string.Compare(usr.Password,password,StringComparison.Ordinal) == 0;
    }
    #endregion
  }
}
