﻿using System;
using System.Collections.Generic;
using System.Linq;
using KMN.Common.Data.Repository;
using KMN.Common.Entities.Enums;
using KMN.Common.Services;

namespace KMN.Service {
  public class LookUpService : BaseService,ILookUpsService {
    readonly IElementsRepo _elements;
    readonly ITagsRepo _tags;
    readonly IUsersRepo _users;

    public LookUpService(IUsersRepo users,ITagsRepo tags,IElementsRepo elements) {
      _users = users;
      _tags = tags;
      _elements = elements;
    }

    #region ILookUpsService Members
    public List<UserInfo> GetUsers() {
      return _users.GetAll().Select(u => new UserInfo {Id = u.Id,ImagePath = u.ImagePath,UserName = u.UserName,LastTimeOnline = u.LastLogin}).ToList();
    }

    public List<ElementInfo> GetElements() {
      //Todo: just it just with my elements!!!
      return _elements.GetAll().Select(e => {
                                         var userPermision = e.UserPermisions.FirstOrDefault(); //p => p.UserId == _app.CurrentUserId);
                                         var permision = userPermision != null ? userPermision.Permision : EPermision.None;
                                         return new ElementInfo {Creation = e.Creation,Creator = GetUserInfoById(e.Creator),CurrentVersion = e.Version,Id = e.Id,Title = e.Title,TotalVersion = e.TotalVersions,Permission = permision,LastEdition = e.LasModification,LastEditor = GetUserInfoById(e.LastEditor),Tags = e.Tags.ToList()};
                                       }).ToList();
    }

    public List<string> GetTags() {
      return _tags.GetAll().Select(t => t.Name).ToList();
    }
    #endregion

    UserInfo GetUserInfoById(Guid id) {
      var usr = _users.GetById(id);
      var response = new UserInfo {Id = usr.Id,UserName = usr.UserName,ImagePath = usr.ImagePath,LastTimeOnline = usr.LastLogin};
      return response;
    }
  }
}
