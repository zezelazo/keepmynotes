﻿using System.Collections.Generic;
using KMN.Common.Services;
using KMN.Common.Services.Dto;

namespace KMN.Service {
  public abstract class BaseService : IBaseService {
    readonly List<ServiceEvent> _events;

    protected BaseService() {
      _events = new List<ServiceEvent>();
    }

    #region IBaseService Members
    public int Ping() {
      return 1;
    }

    public List<ServiceEvent> GetEvents() {
      return _events;
    }
    #endregion

    protected void AddEvent(ServiceEvent eEvent) {
      _events.Add(eEvent);
    }
  }
}
