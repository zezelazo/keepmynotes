﻿using System;
using System.Collections.Generic;
using System.Linq;
using KMN.Common.Data.Repository;
using KMN.Common.Data.Specification;
using KMN.Common.Entities;
using KMN.Common.Services;

namespace KMN.Service {
  class ElementsService : BaseService,IElementsService {
    readonly IElementsRepo _elements;
    readonly IOldElementsRepo _oldElements;
    readonly ITagsRepo _tags;

    public ElementsService(IElementsRepo elements,IOldElementsRepo oldElements,ITagsRepo tags) {
      _elements = elements;
      _oldElements = oldElements;
      _tags = tags;
    }

    //Todo: just it just with my elements!!!

    #region IElementsService Members
    public List<Element> GetElements(ISpecification<Element> filter) {
      if(filter == null) filter = new DirectSpecification<Element>(e => true);
      return _elements.Find(filter).ToList();
    }

    public Element GetElementById(Guid id) {
      return _elements.GetById(id);
    }

    public Element GetElementByTitle(string title) {
      var elements = _elements.Find(new DirectSpecification<Element>(e => e.Title == title));
      return !elements.Any() ? new Element() : elements.FirstOrDefault();
    }

    public Guid AddElement(Element item) {
      ProcessTagsForCreation(item.Tags.ToList());
      return _elements.Add(item);
    }

    public bool EditElement(Element item) {
      ProcessTagsForUpdate(item.Tags.ToList(),_elements.GetById(item.Id).Tags.ToList());
      return _elements.Update(item);
    }

    public bool DeleteElement(Guid id) {
      ProcessTagsForDelete(_elements.GetById(id).Tags.ToList());
      return _elements.Delete(id);
    }

    public List<OldElement> GetElementHistory(Guid elementId) {
      return _oldElements.Find(new DirectSpecification<OldElement>(o => o.ElementId == elementId)).ToList();
    }

    public bool ClearElementHistory(Guid elementId) {
      var elements = _oldElements.Find(new DirectSpecification<OldElement>(o => o.ElementId == elementId)).ToList();
      elements.ForEach(oe => _oldElements.Delete(oe.Id));
      return false;
    }
    #endregion

    #region Tag Management
    public void ProcessTagsForCreation(List<string> newTags) {
      foreach(var tag in newTags) {
        var exTag = _tags.GetByName(tag);
        if(exTag == null) {
          var nTag = new Tag {Name = tag.Trim()};
          _tags.Add(nTag);
        } else {
          exTag.Usages = exTag.Usages + 1;
          _tags.Update(exTag);
        }
      }
    }

    public void ProcessTagsForDelete(List<string> delTags) {
      var delItems = delTags.Select(delItem => _tags.GetByName(delItem)).Where(i => i.Usages > 0).ToList();
      foreach(var item in delItems) {
        item.Usages -= 1;
        _tags.Update(item);
      }
    }

    public void ProcessTagsForUpdate(List<string> newTags,List<string> origTags) {
      var newItems = newTags.Except(origTags).ToList();
      var delItems = origTags.Except(newTags).ToList();
      ProcessTagsForDelete(delItems);
      ProcessTagsForCreation(newItems);
    }
    #endregion
  }
}
