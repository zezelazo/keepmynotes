﻿using System;
using System.Collections.Generic;
using KMN.Common.Entities.Base;
using KMN.Common.Entities.Enums;

namespace KMN.Common.Entities {
  public class Element : Entity<Guid> {
    public string Title { get; set; }
    public EContentType ContentType { get; set; }
    public string Content { get; set; }
    public bool IsPublic { get; set; }
    public virtual ICollection<string> Tags { get; set; }
    public virtual ICollection<ElementProperty> Properties { get; set; }
    public virtual ICollection<UserPermisions> UserPermisions { get; set; }
    public int Version { get; set; }
    public string MaxVersions { get; set; }
    public int TotalVersions { get; set; }
    public virtual ICollection<OldElement> OldVersions { get; set; }
  }
}
