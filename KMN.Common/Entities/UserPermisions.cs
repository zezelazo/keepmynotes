﻿using System;
using KMN.Common.Entities.Base;
using KMN.Common.Entities.Enums;

namespace KMN.Common.Entities {
  public class UserPermisions : Entity<long> {
    public Guid ElementId { get; set; }
    public Guid UserId { get; set; }
    public EPermision Permision { get; set; }
  }
}
