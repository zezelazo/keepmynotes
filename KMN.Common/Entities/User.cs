﻿using System;
using System.Collections.Generic;
using KMN.Common.Entities.Base;

namespace KMN.Common.Entities {
  public class User : Entity<Guid> {
    public string UserName { get; set; }
    public string Password { get; set; }

    public string ImagePath { get; set; }
    public string Email { get; set; }
    public bool Enabled { get; set; }

    public string Token { get; set; }

    public string FacebookLink { get; set; }
    public string TwitterLink { get; set; }
    public string GooglePlusLink { get; set; }

    public DateTime LastLogin { get; set; }
    public DateTime LastLoginError { get; set; }
    public long Notes { get; set; }
    public long PrivateNotes { get; set; }
    public long PublicNotes { get; set; }
    public long Tags { get; set; }

    public virtual  ICollection<UserPermisions> Permisions { get; set; }
  }
}
