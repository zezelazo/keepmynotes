﻿using KMN.Common.Entities.Base;
using KMN.Common.Entities.Enums;

namespace KMN.Common.Entities {
  public class ElementProperty : Entity<long> {
    public long ElementId { get; set; }
    public string  Name { get; set; }
    public EPropetyTypes Type { get; set; }
    public string Value { get; set; }
  }
}
