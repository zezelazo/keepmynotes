﻿using System;
using KMN.Common.Entities.Base;
using KMN.Common.Entities.Enums;

namespace KMN.Common.Entities {
  public class OldElement : Entity<long> {
    public int Version { get; set; }
    public Guid ElementId { get; set; }
    public string Title { get; set; }
    public EContentType ContentType { get; set; }
    public string Content { get; set; }
  }
}
