﻿using KMN.Common.Entities.Base;

namespace KMN.Common.Entities {
  public class Tag : Entity<long> {
    public string Name { get; set; }
    public long Usages { get; set; }

    public Tag() {
      Usages = 1;
    }
  }
}
