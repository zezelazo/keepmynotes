namespace KMN.Common.Entities.Enums {
  public enum EState {
    Added,
    Unchanged,
    Modified,
    Deleted
  }
}
