﻿namespace KMN.Common.Entities.Enums {
  public enum EPermision {
    Owner,
    Read,
    Write,
    None
  }
}
