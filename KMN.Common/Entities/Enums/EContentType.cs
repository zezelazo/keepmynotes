﻿namespace KMN.Common.Entities.Enums {
  public enum EContentType {
    Text,
    Media
  }
}
