﻿namespace KMN.Common.Entities.Enums {
  public enum EPropetyTypes {
    SimpleNumber,
    DecimalNumber,
    String,
    Boolean,
    ImageUrl
  }
}
