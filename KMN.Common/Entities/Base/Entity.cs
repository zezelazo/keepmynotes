﻿using System;
using KMN.Common.Entities.Enums;

namespace KMN.Common.Entities.Base {
  public abstract class Entity<T> : IObjectWithState {
    public T Id { get; set; }

    public Guid Creator { get; set; }
    public Guid LastEditor { get; set; }
    public DateTime Creation { get; set; }
    public DateTime LasModification { get; set; }

    #region IObjectWithState Members
    public EState State { get; set; }
    #endregion
  }
}
