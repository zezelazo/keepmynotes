using KMN.Common.Entities.Enums;

namespace KMN.Common.Entities.Base {
  public interface IObjectWithState {
    EState State { get; set; }
  }
}
