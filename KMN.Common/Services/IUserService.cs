﻿using System;
using System.Collections.Generic;
using KMN.Common.Data.Specification;
using KMN.Common.Entities;

namespace KMN.Common.Services {
  public interface IUserService : IBaseService {
    List<User> GetUsers(ISpecification<User> filter);

    User GetUserById(Guid id);
    User GetUserByName(string userName);

    Guid AddUser(User item);
    bool EditUser(User item);
    bool DisableUser(Guid id);
    bool EnableUser(Guid id);

    bool DeleteUser(Guid id);
    bool VerifiedPassword(Guid id,string password);
  }
}
