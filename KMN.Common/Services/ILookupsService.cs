﻿using System.Collections.Generic;

namespace KMN.Common.Services {
  public interface ILookUpsService : IBaseService {
    List<UserInfo> GetUsers();
    List<ElementInfo> GetElements();
    List<string> GetTags();
  }
}
