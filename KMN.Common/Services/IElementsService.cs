﻿using System;
using System.Collections.Generic;
using KMN.Common.Data.Specification;
using KMN.Common.Entities;

namespace KMN.Common.Services {
  public interface IElementsService : IBaseService {
    List<Element> GetElements(ISpecification<Element> filter);

    Element GetElementById(Guid id);
    Element GetElementByTitle(string title);

    Guid AddElement(Element item);
    bool EditElement(Element item);
    bool DeleteElement(Guid id);

    List<OldElement> GetElementHistory(Guid elementId);
    bool ClearElementHistory(Guid elementId);
  }
}
