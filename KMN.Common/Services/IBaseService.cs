﻿using System.Collections.Generic;
using KMN.Common.Services.Dto;

namespace KMN.Common.Services {
  public interface IBaseService {
    int Ping();
    List<ServiceEvent> GetEvents();
  }
}
