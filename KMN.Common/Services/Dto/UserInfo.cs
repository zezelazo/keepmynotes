﻿using System;

namespace KMN.Common.Services {
  public class UserInfo {
    public Guid Id { get; set; }
    public string UserName { get; set; }
    public string ImagePath { get; set; }
    public DateTime LastTimeOnline { get; set; }
  }
}
