﻿using System;
using System.Collections.Generic;
using KMN.Common.Entities.Enums;

namespace KMN.Common.Services {
  public class ElementInfo {
    public Guid Id { get; set; }
    public string Title { get; set; }
    public DateTime Creation { get; set; }
    public DateTime LastEdition { get; set; }
    public List<string> Tags { get; set; }
    public int CurrentVersion { get; set; }
    public int TotalVersion { get; set; }
    public UserInfo Creator { get; set; }
    public UserInfo LastEditor { get; set; }
    public EPermision Permission { get; set; }
  }
}
