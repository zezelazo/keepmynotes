﻿using System;
using KMN.Common.Entities;

namespace KMN.Common.Services.Dto {
  public abstract class ServiceEvent {
    public long Id { get; set; }
    public string Description { get; set; }
    public DateTime TimeStamp { get; set; }
    public User Creator { get; set; }
  }

  public class ServiceError : ServiceEvent {
    public Exception Exception { get; set; }
  }

  public class ServiceInfo : ServiceEvent {
    public string Details { get; set; }
  } }
