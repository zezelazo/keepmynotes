﻿using System;

namespace KMN.Common {
  public class AppConfig {


    static readonly Lazy<AppConfig> Obj = new Lazy<AppConfig>(() => new AppConfig());


    readonly string _connectionString;
    Guid _currentUserId;


    AppConfig()
    {
      _connectionString = "KMN.sdf";
      _currentUserId = Guid.Empty;
    }

    public string ConnectionString
    {
      get { return _connectionString; }
    }

    public Guid CurrentUserId
    {
      get { return _currentUserId; }
    }

    public void SetCurrentUser(Guid id)
    {
      _currentUserId = id;
    }

    public static AppConfig Instance
    {
      get { return Obj.Value; }
    }
  }
}
