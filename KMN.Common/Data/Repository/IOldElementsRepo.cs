﻿using KMN.Common.Entities;

namespace KMN.Common.Data.Repository {
  public interface IOldElementsRepo : IBaseRepo<long,OldElement> {}
}
