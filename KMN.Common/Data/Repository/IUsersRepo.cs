﻿using System;
using KMN.Common.Entities;

namespace KMN.Common.Data.Repository {
  public interface IUsersRepo : IBaseRepo<Guid,User> {}
}
