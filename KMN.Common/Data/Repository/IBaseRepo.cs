﻿using System.Collections.Generic;
using KMN.Common.Data.Specification;
using KMN.Common.Entities;
using KMN.Common.Entities.Base;

namespace KMN.Common.Data.Repository {
  public interface IBaseRepo<TKey,TEntity> where TEntity : Entity<TKey> {
    TEntity GetById(TKey id);
    IEnumerable<TEntity> GetAll();
    IEnumerable<TEntity> Find(ISpecification<TEntity> specification);

    long Count(ISpecification<TEntity> specification = null);

    TKey Add(TEntity item);
    bool Update(TEntity item);
    bool Delete(TKey id);
  }
}
