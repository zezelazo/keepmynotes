﻿using KMN.Common.Entities;

namespace KMN.Common.Data.Repository {
  public interface ITagsRepo : IBaseRepo<long,Tag> {
    Tag GetByName(string name);
  }
}
