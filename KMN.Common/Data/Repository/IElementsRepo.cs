﻿using System;
using KMN.Common.Entities;

namespace KMN.Common.Data.Repository {
  public interface IElementsRepo : IBaseRepo<Guid,Element> {}
}
