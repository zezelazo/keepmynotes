﻿using System;

namespace KMN.Common.Data.Specification {
  public abstract class CompositeSpecification<TEntity> : Specification<TEntity> {
    protected CompositeSpecification(ISpecification<TEntity> leftSide,ISpecification<TEntity> rightSide) {
      if(leftSide == null) throw new ArgumentNullException("leftSide");
      if(rightSide == null) throw new ArgumentNullException("rightSide");
      LeftSideSpecification = leftSide;
      RightSideSpecification = rightSide;
    }

    public ISpecification<TEntity> LeftSideSpecification { get; internal set; }

    public ISpecification<TEntity> RightSideSpecification { get; internal set; }
  }
}
