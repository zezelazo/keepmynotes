﻿namespace KMN.Common.Data.Specification {
  public static class SpecificationExtensionMethods {
    public static ISpecification<TEntity> And<TEntity>(this ISpecification<TEntity> specificationOne,ISpecification<TEntity> specificationTwo) {
      return new AndSpecification<TEntity>(specificationOne,specificationTwo);
    }

    public static ISpecification<TEntity> Or<TEntity>(this ISpecification<TEntity> specificationOne,ISpecification<TEntity> specificationTwo) {
      return new OrSpecification<TEntity>(specificationOne,specificationTwo);
    }

    public static ISpecification<TEntity> Not<TEntity>(this ISpecification<TEntity> specification) {
      return new NotSpecification<TEntity>(specification);
    }
  }
}
