﻿namespace KMN.Common.Data.Specification {
  sealed class OrSpecification<TEntity> : CompositeSpecification<TEntity> {
    public OrSpecification(ISpecification<TEntity> leftSide,ISpecification<TEntity> rightSide) : base(leftSide,rightSide) {}

    public override bool SatisfiedBy(TEntity entity) {
      return (LeftSideSpecification.SatisfiedBy(entity) || RightSideSpecification.SatisfiedBy(entity));
    }
  }
}
