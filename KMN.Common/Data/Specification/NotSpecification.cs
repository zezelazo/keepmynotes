﻿using System;

namespace KMN.Common.Data.Specification {
  sealed class NotSpecification<TEntity> : Specification<TEntity> {
    readonly ISpecification<TEntity> _originalCriteria;

    public NotSpecification(ISpecification<TEntity> originalSpecification) {
      if(originalSpecification == null) throw new ArgumentNullException("originalSpecification");
      _originalCriteria = originalSpecification;
    }

    public override bool SatisfiedBy(TEntity entity) {
      return !(_originalCriteria.SatisfiedBy(entity));
    }
  }
}
