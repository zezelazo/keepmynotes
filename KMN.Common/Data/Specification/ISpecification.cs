﻿namespace KMN.Common.Data.Specification {
  public interface ISpecification<in TEntity> {
    bool SatisfiedBy(TEntity entity);
  }
}
