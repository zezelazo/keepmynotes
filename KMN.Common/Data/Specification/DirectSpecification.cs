﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KMN.Common.Data.Specification
{
  public class DirectSpecification<TEntity> :  Specification<TEntity>
  {
    readonly Func<TEntity,bool> _rule;

    public DirectSpecification(Func<TEntity,bool> rule) {
      _rule = rule;
    }

    public override bool SatisfiedBy(TEntity entity)
    {
      return _rule(entity);
    }
  }
}
