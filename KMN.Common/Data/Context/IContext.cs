﻿using System.Collections.Generic;

namespace KMN.Common.Data.Context {
  public interface IContext {                            
    int SaveChanges();
  }
}
